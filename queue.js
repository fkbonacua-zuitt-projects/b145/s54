let collection = [];

// Write the queue functions below.


// 1. Output all the elements of the queue
function print() {
	return [... collection]
};

// 2. Adds element to the rear of the queue
const enqueue = (stack) => {
	collection.push(stack)
	return collection
};

// other methods
// function enqueue(e){
// 	collection = [... collection, e]
// 	return collection;
// }

// 3. Removes element from the front of the queue
const dequeue =(stack) => {
	collection.shift(stack)
	return collection
};
// other methods
// function dequeue(e){
// 	collection.splice(0,1);
// 	return collection;
// }

// 4. Show element at the front
function front () {
	return collection[0]
};

// 5. Show the total number of elements
function size () {
	return collection.length;
};

// 6. Outputs a Boolean value describing whether queue is empty or not
function isEmpty() {
	return collection.length === 0;
};

// 7. Export your functions
module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};